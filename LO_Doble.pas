unit LO_Doble;

interface

uses
  SysUtils,dialogs;

const
  _PosNula = -1;

type
  tipoposicion = longint;
  tipoenlace = longint;
  tipoclave = string[10];
  puntos = integer;
  _estado = byte;

  RD = record
    clave: tipoclave;//apodo
    Puntaje: puntos;
    Estado: _estado;
    Ant,Sig: tipoenlace;
    end;

  RC = record
      primero,ultimo,borrado: tipoenlace;
      end;

  TRD = file of RD;
  TRC = file of RC;
  Jugadores = record
              C: TRC;
              D: TRD;
              end;

var
  Doble:Jugadores;

  procedure CrearDoble(var Doble:Jugadores; nombre,ruta:string);
  procedure AbrirDoble(var Doble:Jugadores);
  procedure CerrarDoble(var Doble:Jugadores);
  procedure DestruirDoble(var Doble:Jugadores; nombre:string);
  function Buscar(var Doble:Jugadores; clave:tipoclave; var pos:tipoposicion):boolean;
  procedure Eliminar(var Doble:Jugadores; pos:tipoposicion);
  procedure Insertar(var Doble:Jugadores; pos:tipoposicion; Reg:rd);
  procedure Capturar(var Doble:Jugadores; pos:tipoposicion; var Reg:rd);
  procedure Modificar(var Doble:Jugadores; pos:tipoposicion; Reg:rd);
  function Primero(var Doble:Jugadores):tipoposicion;
  function Ultimo(var Doble:Jugadores):tipoposicion;
  function Proximo(var Doble:Jugadores; pos:tipoposicion):tipoposicion;
  function Anterior(var Doble:Jugadores; pos:tipoposicion):tipoposicion;
  function DobleVacio(var Doble:Jugadores):boolean;
  function PosNula(var Doble:Jugadores):tipoposicion;


implementation

  procedure CrearDoble(var Doble:Jugadores; nombre,ruta:string);
  var
  bExtCon,bExtDat:boolean; R:RC;
  begin
    Assignfile(Doble.C, ruta+nombre+'.con');
    Assignfile(Doble.D, ruta+nombre+'.dat');
    bExtCon:= fileexists(ruta+nombre+'.con');
    bExtDat:= fileexists(ruta+nombre+'.dat');
    if not(bExtCon) or not(bExtDat)
    then begin
     R.primero:= _PosNula;
     R.ultimo:= _PosNula;
     R.borrado:= _PosNula;
     rewrite(Doble.C);
     write(Doble.C,R);
     rewrite(Doble.D);
     close(Doble.C);
     close(Doble.D);
    end;//if
  end;//procedure

  procedure AbrirDoble(var Doble:Jugadores);
  begin
    reset(Doble.C);
    reset(Doble.D);
  end;

  procedure CerrarDoble(var Doble:Jugadores);
  begin
    close(Doble.C);
    close(Doble.D);
  end;

  procedure DestruirDoble(var Doble:Jugadores; nombre:string);
  begin
    Assignfile(Doble.C,nombre+'.con');
    Assignfile(Doble.D,nombre+'.dat');
    deletefile(nombre+'.con');
    deletefile(nombre+'.dat');
  end;

  function Buscar(var Doble:Jugadores; clave:tipoclave; var pos:tipoposicion):boolean;
  var encontrado,corte:boolean; Reg:RD; R:RC;
  begin
    seek(Doble.C,0);
    read(Doble.C,R);
    pos:=R.primero;
    encontrado:=false;
    corte:=false;
    while not(corte) and not(encontrado) and (pos<>_PosNula)
    do begin
      seek(Doble.D,pos);
      read(Doble.D,Reg);
      if Reg.clave=clave then
        encontrado := true
      else if Reg.clave>clave then
        corte := true
      else
        pos := Reg.Sig;
    end;//while
  Buscar := encontrado;
  end;//fBuscar

  procedure Eliminar(var Doble:Jugadores; pos:tipoposicion);
  var Reg,Reg1,Reg2:RD; R:RC; pos2,pos1:tipoposicion;
  begin
    seek(Doble.C,0);
    read(Doble.C,R);
    seek(Doble.D,pos);
    read(Doble.D,Reg);
    if (R.primero=pos) and (R.ultimo=pos)
    then begin
      R.primero:=_PosNula;
      R.ultimo:=_PosNula;
    end
    else if R.primero=pos
    then begin
      pos2:=Reg.Sig;
      seek(Doble.D,pos2);
      read(Doble.D,Reg2);
      Reg2.Ant:=_PosNula;
      R.primero:=pos2;
      seek(Doble.D,pos2);
      write(Doble.D,Reg2);
    end
    else if R.ultimo=pos
    then begin
      pos1:=Reg.Ant;
      seek(Doble.D,pos1);
      read(Doble.D,Reg1);
      Reg1.Sig:=_PosNula;
      seek(Doble.D,pos1);
      write(Doble.D,Reg1);
      R.ultimo:=pos1;
    end
    else begin
      pos1:=Reg.Ant;
      pos2:=Reg.Sig;
      seek(Doble.D,pos1);
      read(Doble.D,Reg1);
      seek(Doble.D,pos2);
      read(Doble.D,Reg2);
      Reg1.Sig:=pos2;
      Reg2.Ant:=pos1;
      seek(Doble.D,pos1);
      write(Doble.D,Reg1);
      seek(Doble.D,pos2);
      write(Doble.D,Reg2);
    end;
    //envio el reg a borrados
    Reg.Sig:=R.borrado;
    Reg.Ant:=_PosNula;
    R.borrado:=pos;
    seek(Doble.D,pos);
    write(Doble.D,Reg);
    //grabo cabecera
    seek(Doble.C,0);
    write(Doble.C,R);
  end;//PEliminar

  procedure Insertar(var Doble:Jugadores; pos:tipoposicion; Reg:rd);
  var R:RC; posNueva,pos1:tipoposicion; RAux,Reg1,Reg2:RD;
  begin
    seek(Doble.C,0);
    read(Doble.C,R);
    if R.borrado=_PosNula
    then posNueva:=FileSize(Doble.D)
    else begin
      posNueva:=R.borrado;
      seek(Doble.D,posNueva);
      read(Doble.D,RAux);
      R.borrado:=RAux.Sig;
    end;
    if R.primero=_PosNula
    then begin
      Reg.Sig:=_PosNula;
      Reg.Ant:=_PosNula;
      R.primero:=posNueva;
      R.ultimo:=posNueva;
    end
    else if R.primero=pos
    then begin
      Reg.Ant:=_PosNula;
      Reg.Sig:=R.primero;
      seek(Doble.D,pos);
      read(Doble.D,RAux);
      RAux.Ant:=posNueva;
      R.primero:=posNueva;
      seek(Doble.D,pos);
      write(Doble.D,RAux);
    end  //----
    else if pos=_PosNula
    then begin
      Reg.Sig:=_PosNula;
      Reg.Ant:=R.ultimo;
      seek(Doble.D,R.ultimo);
      read(Doble.D,RAux);
      RAux.Sig:=posNueva;
      R.ultimo:=posNueva;
      seek(Doble.D,Reg.Ant);
      write(Doble.D,RAux);
    end
    else begin
      seek(Doble.D,pos);
      read(Doble.D,Reg2);
      pos1:=Reg2.Ant;
      seek(Doble.D,pos1);
      read(Doble.D,Reg1);
      Reg1.Sig:=posNueva;
      Reg2.Ant:=posNueva;
      Reg.Sig:=pos;
      Reg.Ant:=pos1;
      seek(Doble.D,pos1);
      write(Doble.D,Reg1);
      seek(Doble.D,pos);
      write(Doble.D,Reg2);
    end;
    //grabo cabecera
    seek(Doble.C,0);
    write(Doble.C,R);
    //grabo insercion
    seek(Doble.D,posNueva);
    write(Doble.D,Reg);
  end;//PInsertar

  procedure Capturar(var Doble:Jugadores; pos:tipoposicion; var Reg:rd);
  begin
    seek(Doble.D,pos);
    read(Doble.D,Reg);
  end;//PCapturar

  procedure Modificar(var Doble:Jugadores; pos:tipoposicion; Reg:rd);
  var RAux:RD;
  begin
    seek(Doble.D,pos);
    read(Doble.D,RAux);
    Reg.Ant:=RAux.Ant;
    Reg.Sig:=RAux.Sig;
    seek(Doble.D,pos);
    write(Doble.D,Reg);
  end;//PModificar

  function Primero(var Doble:Jugadores):tipoposicion;
  var R:RC;
  begin
    seek(Doble.C,0);
    read(Doble.C,R);
    Primero:=R.primero;
  end;//FPrimero

  function Ultimo(var Doble:Jugadores):tipoposicion;
  var R:RC;
  begin
    seek(Doble.C,0);
    read(Doble.C,R);
    Ultimo:=R.ultimo;
  end;//FUltimo

  function Proximo(var Doble:Jugadores; pos:tipoposicion):tipoposicion;
  var Reg:RD;
  begin
    seek(Doble.D,pos);
    read(Doble.D,Reg);
    Proximo:=Reg.Sig;
  end;//FProximo

  function Anterior(var Doble:Jugadores; pos:tipoposicion):tipoposicion;
  var Reg:RD;
  begin
    seek(Doble.D,pos);
    read(Doble.D,Reg);
    Anterior:=Reg.Ant;
  end;//FProximo

  function DobleVacio(var Doble:Jugadores):boolean;
  var
    R:RC;
  begin
    seek(Doble.C,0);
    read(Doble.C,R);
    DobleVacio:=(R.primero=_PosNula);
  end;

  function PosNula(var Doble:Jugadores):tipoposicion;
  begin
  posnula:=_posnula;
  end;

end.
