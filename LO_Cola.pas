unit LO_Cola;

interface

uses
  SysUtils,dialogs;

const
  _PosNula = -1;

type
  tcolor = string[6];
  tipoposicion = longint;
  tipoenlace = longint;

  RD = record
    color: tcolor;
    numero: byte;
    enlace: tipoenlace;
    end;
  RC = record
    primero,ultimo,borrado: tipoenlace;
    total: byte;
    end;
  TRD = file of RD;
  TRC = file of RC;
  TipoCola = record
              C: TRC;
              D: TRD;
              end;

var
  Cola:TipoCola;

  procedure CrearCola(var Cola:TipoCola; nombre,ruta:string);
  procedure AbrirCola(var Cola:TipoCola);
  procedure CerrarCola(var Cola:TipoCola);
  procedure DestruirCola(var Cola:tipoCola; nombre:string);
  function ColaVacia(var Cola:TipoCola):boolean;
  procedure Frente(var Cola:TipoCola; var Reg:RD);
  procedure Decolar(var Cola:TipoCola);
  procedure Encolar(var Cola:TipoCola; Reg:RD);
  function PosNula(var Cola:TipoCola):tipoposicion;

implementation

  procedure CrearCola(var Cola:TipoCola; nombre,ruta:string);
  var
  bExtCon,bExtDat:boolean; R:RC;
  begin
    Assignfile(Cola.C, ruta+nombre+'.con');
    Assignfile(Cola.D, ruta+nombre+'.dat');
    bExtCon:= fileexists(ruta+nombre+'.con');
    bExtDat:= fileexists(ruta+nombre+'.dat');
    if not(bExtCon) or not(bExtDat)
    then begin
     R.primero:= _PosNula;
     R.ultimo:= _PosNula;
     R.borrado:= _PosNula;
     R.total:=0;
     rewrite(Cola.C);
     write(Cola.C,R);
     rewrite(Cola.D);
     close(Cola.C);
     close(Cola.D);
    end;//if
  end;//procedure

  procedure AbrirCola(var Cola:TipoCola);
  begin
    reset(Cola.C);
    reset(Cola.D);
  end;

  procedure CerrarCola(var Cola:TipoCola);
  begin
    close(Cola.C);
    close(Cola.D);
  end;

  procedure DestruirCola(var Cola:tipoCola; nombre:string);
  begin
    Assignfile(Cola.C,nombre+'.con');
    Assignfile(Cola.D,nombre+'.dat');
    deletefile(nombre+'.con');
    deletefile(nombre+'.dat');
  end;

  function ColaVacia(var Cola:TipoCola):boolean;
  var
    R:RC;
  begin
    seek(Cola.C,0);
    read(Cola.C,R);
    ColaVacia:= (R.primero=_PosNula);
  end;

  procedure Frente(var Cola:TipoCola; var Reg:RD);
  var
    R:RC;
  begin
    seek(Cola.C,0);
    read(Cola.C,R);
    seek(Cola.D,R.primero);
    read(Cola.D,Reg);
  end;

  procedure Decolar(var Cola:TipoCola);
  var
    R:RC; pos:tipoposicion; Reg:RD;
  begin
    seek(Cola.C,0);
    read(Cola.C,R);
    pos:= R.primero;
    seek(Cola.D,pos);
    read(Cola.D,Reg);
    R.primero:= Reg.enlace;
    if Reg.enlace=_PosNula
    then R.ultimo:=_PosNula;
    Reg.enlace:= R.borrado;
    R.borrado:= pos;
    R.total:=R.total-1;
    seek(Cola.C,0);
    write(Cola.C,R);
    seek(Cola.D,pos);
    write(Cola.D,Reg);
  end;

  procedure Encolar(var Cola:TipoCola; Reg:RD);
  var
    R:RC; pos:tipoposicion; RAux:RD;
  begin
    seek(Cola.C,0);
    read(Cola.C,R);
    if (R.borrado<>_PosNula)
    then begin
      pos:= R.borrado;
      seek(Cola.D,pos);
      read(Cola.D,RAux);
      R.borrado:= RAux.enlace;
      end
    else
      pos:= filesize(Cola.D);
    if (R.primero=_PosNula)
    then begin
      Reg.enlace:=_PosNula;
      R.primero:=pos;
      R.ultimo:=pos;
      end
    else begin
      seek(Cola.D,R.ultimo);
      read(Cola.D,RAux);
      RAux.enlace:=pos;
      seek(Cola.D,R.ultimo);
      write(Cola.D,RAux);
      R.ultimo:=pos;
      Reg.enlace:=_PosNula;
    end;
    R.total:=R.total+1;
    seek(Cola.C,0);
    write(Cola.C,R);
    seek(Cola.D,pos);
    write(Cola.D,Reg);
  end;

  function PosNula(var Cola:TipoCola):tipoposicion;
  begin
  posnula:=_posnula;
  end;

end.
