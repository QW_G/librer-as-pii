unit LO_Pila;

interface

uses
  SysUtils,dialogs;

const
  _PosNula = -1;

type
  tcolor = string[6];
  tipoposicion = longint;
  tipoenlace = longint;

  RD = record
    color: tcolor;
    numero: byte;
    enlace: tipoenlace;
    end;
  RC = record
    primero,borrado: tipoenlace;
    total: byte;
    end;
  TRD = file of RD;
  TRC = file of RC;
  TipoPila = record
              C: TRC;
              D: TRD;
              end;

var
  Pila:TipoPila;

  procedure CrearPila(var Pila:TipoPila; nombre,ruta:string);
  procedure AbrirPila(var Pila:TipoPila);
  procedure CerrarPila(var Pila:TipoPila);
  procedure DestruirPila(var PilaTipopila; nombre:string);  //nuevo
  function PilaVacia(var Pila:TipoPila):boolean;
  procedure Tope(var Pila:TipoPila; var Reg:RD);
  procedure Desapilar(var Pila:TipoPila);
  procedure Apilar(var Pila:TipoPila; Reg:RD);
  function PosNula(var Pila:TipoPila):tipoposicion; ///nuevo

implementation

  procedure CrearPila(var Pila:TipoPila; nombre,ruta:string);
  var
  bExtCon,bExtDat:boolean; R:RC;
  begin
    Assignfile(Pila.C, ruta+nombre+'.con');
    Assignfile(Pila.D, ruta+nombre+'.dat');
    bExtCon:= fileexists(ruta+nombre+'.con');
    bExtDat:= fileexists(ruta+nombre+'.dat');
    if not(bExtCon) or not(bExtDat)
    then begin
     R.primero:= _PosNula;
     R.borrado:= _PosNula;
     R.total:=0;
     rewrite(Pila.C);
     write(Pila.C,R);
     rewrite(Pila.D);
     close(Pila.C);
     close(Pila.D);
    end;//if
  end;//procedure

  procedure AbrirPila(var Pila:TipoPila);
  begin
    reset(Pila.C);
    reset(Pila.D);
  end;

  procedure CerrarPila(var Pila:TipoPila);
  begin
    close(Pila.C);
    close(Pila.D);
  end;

  procedure DestruirPila(var PilaTipopila; nombre:string);
  begin
    Assignfile(Pila.C,nombre+'.con');
    Assignfile(Pila.D,nombre+'.dat');
    deletefile(nombre+'.con');
    deletefile(nombre+'.dat');
  end;

  function PilaVacia(var Pila:TipoPila):boolean;
  var
    R:RC;
  begin
    seek(Pila.C,0);
    read(Pila.C,R);
    PilaVacia:= (R.primero=_PosNula);
  end;

  procedure Tope(var Pila:TipoPila; var Reg:RD);
  var
    R:RC;
  begin
    seek(Pila.C,0);
    read(Pila.C,R);
    seek(Pila.D,R.primero);
    read(Pila.D,Reg);
  end;

  procedure Desapilar(var Pila:TipoPila);
  var
    R:RC; pos:tipoposicion; Reg:RD;
  begin
    seek(Pila.C,0);
    read(Pila.C,R);
    pos:= R.primero;
    seek(Pila.D,pos);
    read(Pila.D,Reg);
    R.primero:= Reg.enlace;
    Reg.enlace:= R.borrado;
    R.borrado:= pos;
    R.total:= R.total-1;
    seek(Pila.C,0);
    write(Pila.C,R);
    seek(Pila.D,pos);
    write(Pila.D,Reg);
  end;

  procedure Apilar(var Pila:TipoPila; Reg:RD);
  var
    R:RC; pos:tipoposicion; RAux:RD;
  begin
    seek(Pila.C,0);
    read(Pila.C,R);
    if (R.borrado<>_PosNula)
    then begin
      pos:= R.borrado;
      seek(Pila.D,pos);
      read(Pila.D,RAux);
      R.borrado:= RAux.enlace;
      end
    else
      pos:= filesize(Pila.D);
    Reg.enlace:= R.primero;
    R.primero:= pos;
    R.total:=R.total+1;
    seek(Pila.C,0);
    write(Pila.C,R);
    seek(Pila.D,pos);
    write(Pila.D,Reg);
  end;

  function PosNula(var Pila:TipoPila):tipoposicion;
  begin
  posnula:=_posnula;
  end;

  
end.
